package ExamEx1;

public class Ex1 {
    public static void main(String[] args) {
        NiceThread niceThread1 = new NiceThread(" NiceThread1");
        NiceThread niceThread2 = new NiceThread(" NiceThread2");
        NiceThread niceThread3 = new NiceThread(" NiceThread3");


        niceThread1.start();
        niceThread2.start();
        niceThread3.start();
    }
}


class NiceThread implements Runnable {

    private Thread thread;

    NiceThread(String name) {
        thread = new Thread(this);
        thread.setName(name);
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this);

        }
        thread.start();
    }

    public void run() {

        for (int i = 1; i <= 9; i++) {
            try {
                System.out.println(thread.getName() +" - "+ i);
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}






